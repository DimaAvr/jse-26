package ru.tsc.avramenko.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.IRepository;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {

    @NotNull
    List<Task> findAll(@NotNull String userId, @NotNull Comparator<Task> comparator);

    boolean existsById(@NotNull String userId, @NotNull String id);

    @NotNull
    List<Task> findAllTaskByProjectId(@NotNull String userId, @NotNull final String id);

    @NotNull
    Task bindTaskToProjectById(@NotNull String userId, @NotNull final String projectId, @NotNull final String taskId);

    @NotNull
    Task unbindTaskById(@NotNull String userId, @NotNull final String id);

    @NotNull
    void unbindAllTaskByProjectId(@NotNull String userId, @NotNull final String id);

    @NotNull
    Task findByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Task findByIndex(@NotNull String userId, @NotNull int index);

    @Nullable
    Task removeByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task removeByIndex(@NotNull String userId, @NotNull int index);

    @Nullable
    Task startById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task startByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task startByIndex(@NotNull String userId, @NotNull int index);

    @Nullable
    Task finishById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task finishByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task finishByIndex(@NotNull String userId, @NotNull int index);

    @Nullable
    Task changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @Nullable
    Task changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status);

    @Nullable
    Task changeStatusByIndex(@NotNull String userId, @NotNull int index, @NotNull Status status);

}
